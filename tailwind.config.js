module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        "black": "#000000",
        "dark-mode": "#333333",
        "dark-card": "#3B3B3C",
        "dark-grey": "#999999",
        "dark-grey-1": "#4D4E50",
        "dark-grey-2": "#D1D5DB",
        "dark-coal": "#2B2B2B",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
