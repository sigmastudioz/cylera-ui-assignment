/** @type {import('next').NextConfig} */
const path = require("path");

module.exports = {
  reactStrictMode: true,
  async rewrites() {
    return [
      {
        source: "/api/graphql",
        destination: "https://api.spacex.land/graphql", // Proxy to Backend
      },
    ];
  },

  webpack(config) {
    config.resolve.modules.push(path.resolve("./"));
    return config;
  },
};
