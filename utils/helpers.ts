export interface payloadData {
  customers?: [string];
  manufacturer?: string;
  nationality: string;
  payload_mass_lbs?: number | undefined;
  count: number;
}

export interface payloadCountByCountryOptions {
  [key: string]: payloadData;
}

export const ascendingOrder = (
  a: any,
  b: any,
  type: string = "count"
): number => {
  const prev = a[type];
  const next = b[type];

  if (prev < next) {
    return 1;
  }

  if (prev > next) {
    return -1;
  }

  return 0;
};

export const payloadFilter = (
  payloads: [payloadData],
  start: number = 0,
  total: number = 5
) => {
  const topPayloads = payloads?.filter((a: payloadData) => a.nationality);
  const payloadByCountry: payloadCountByCountryOptions = {};
  let customers: any = [];
  topPayloads?.forEach((payload: payloadData) => {
    let country: payloadData = payloadByCountry[payload.nationality];

    payload.customers?.forEach((customer: string) => {
      if (customers.indexOf(customer) === -1) {
        customers.push(customer);
      }
    });

    if (typeof country === "undefined") {
      country = {
        nationality: payload.nationality,
        manufacturer: payload.manufacturer,
        count: 1,
      };
    } else {
      country.count += 1;
    }
    payloadByCountry[payload.nationality] = country;
  });

  return {
    total: topPayloads.length - 1,
    customers,
    payloads: Object.keys(payloadByCountry)
      .map((nationality: string) => {
        return payloadByCountry[nationality];
      })
      ?.sort((a, b) => ascendingOrder(a, b, "count"))
      .splice(start, total),
  };
};
