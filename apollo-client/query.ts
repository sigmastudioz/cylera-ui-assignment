import { gql } from "@apollo/client";
import client from "@apollo-client/index";

export const Payloads = async () =>
  await client.query({
    query: gql`
      query Payloads {
        payloads {
          nationality
          manufacturer
          payload_mass_kg
          customers
        }
      }
    `,
  });

export const Missions = async () =>
  await client.query({
    query: gql`
      query Missions {
        missions {
          name
          payloads {
            payload_mass_kg
          }
        }
      }
    `,
  });

export const Launches = async (variables?: {
  find?: {
    mission_name?: string;
  };
  sort?: string;
  order?: string;
  limit?: number;
  offset?: number;
}) =>
  await client.query({
    query: gql`
      query Launches(
        $find: LaunchFind
        $sort: String = "launch_date_utc"
        $order: String = "asc"
        $limit: Int = 6
        $offset: Int = 0
      ) {
        launches(
          find: $find,
          sort: $sort, 
          order: $order, 
          limit: $limit, 
          offset: $offset
        ) {
          launch_site {
            site_id
          }
          mission_id
          launch_success
          launch_year
          mission_name
          launch_date_utc
          rocket {
            rocket_name
            rocket {
              mass {
                kg
              }
            }
          }
        }
      }
    `,
    variables,
  });
