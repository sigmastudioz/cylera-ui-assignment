import { ApolloClient, InMemoryCache } from "@apollo/client";

const client = new ApolloClient({
  uri: `https://api.spacex.land/graphql`, //http://localhost:${process.env.PORT || 3000}/api/graphql
  cache: new InMemoryCache(),
});

export default client;
