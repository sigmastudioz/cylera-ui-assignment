import { Fragment } from "react";
import { Disclosure, Menu, Switch, Transition } from "@headlessui/react";
import {
  ChevronDownIcon,
  CogIcon,
  OfficeBuildingIcon,
} from "@heroicons/react/outline";

import HeadLineText from "@components/HeadLineText/HeadLineText";
import classNames from "classnames";
const userNavigation = [{ name: "Log out", href: "#" }];

const Navigation: React.FC<{ toggleDark?: Function; isDark?: boolean }> = ({
  toggleDark = () => {},
  isDark = false,
}) => {
  const bgColor = "bg-white dark:bg-dark-coal";

  return (
    <Disclosure
      as="nav"
      className="bg-gradient-to-b from-gray-50 via-gray-50 dark:from-dark-mode dark:via-dark-mode fixed left-0 right-0 top-0 z-50 pt-8 pb-4"
    >
      {({ open }) => (
        <>
          <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div className="flex justify-between h-16">
              <div className="flex">
                <div className="md:flex md:items-center md:space-x-4">
                  <HeadLineText
                    title={<span>Space X Mission Dashboard</span>}
                  />
                </div>
              </div>

              <div className="flex items-center">
                <div className="hidden md:ml-4 md:flex-shrink-0 md:flex md:items-center">
                  <Menu as="div" className="ml-3 relative">
                    <div>
                      <Menu.Button className="bg-blue-500 shadow p-1 rounded-full text-white focus:outline-none p-3">
                        <CogIcon className="h-4 w-4" aria-hidden="true" />
                      </Menu.Button>
                    </div>
                    <Transition
                      as={Fragment}
                      enter="transition ease-out duration-200"
                      enterFrom="transform opacity-0 scale-95"
                      enterTo="transform opacity-100 scale-100"
                      leave="transition ease-in duration-75"
                      leaveFrom="transform opacity-100 scale-100"
                      leaveTo="transform opacity-0 scale-95"
                    >
                      <Menu.Items
                        className={classNames(
                          bgColor,
                          "origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg py-1  ring-1 ring-black ring-opacity-5 focus:outline-none"
                        )}
                      >
                        <Menu.Item key={`light-dark`}>
                          <Switch.Group
                            as="div"
                            className="flex items-center block px-4 py-2 border-b-2 border-gray-50 dark:border-dark-card"
                          >
                            <Switch.Label as="span" className="mr-3">
                              <span className="text-sm text-gray-700 dark:text-dark-grey">
                                Light / Dark Theme
                              </span>
                            </Switch.Label>
                            <Switch
                              checked={isDark}
                              onChange={(value) => toggleDark(value)}
                              className={classNames(
                                isDark ? "bg-blue-500" : "bg-gray-200",
                                "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                              )}
                            >
                              <span
                                aria-hidden="true"
                                className={classNames(
                                  isDark ? "translate-x-5" : "translate-x-0",
                                  "pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                                )}
                              />
                            </Switch>
                          </Switch.Group>
                        </Menu.Item>
                        {userNavigation.map((item, index) => (
                          <Menu.Item key={item.name}>
                            {({ active }) => (
                              <a
                                href={item.href}
                                className={classNames(
                                  index !== userNavigation.length - 1
                                    ? "border-gray-50 border-b-2 dark:border-dark-card"
                                    : "",
                                  "block px-4 py-2 text-sm text-gray-700 dark:text-dark-grey"
                                )}
                              >
                                {item.name}
                              </a>
                            )}
                          </Menu.Item>
                        ))}
                      </Menu.Items>
                    </Transition>
                  </Menu>
                </div>

                <div className="flex-shrink-0 ml-8">
                  <button
                    type="button"
                    className={classNames(
                      bgColor,
                      "relative inline-flex items-center w-52 px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-blue-500 dark:text-white"
                    )}
                  >
                    <OfficeBuildingIcon
                      className="-ml-1 mr-4 h-4 w-4"
                      aria-hidden="true"
                    />
                    <span className="flex-grow text-left text-xs">Launch Site</span>
                    <ChevronDownIcon
                      className="-mr-1 ml-4 h-4 w-4"
                      aria-hidden="true"
                    />
                  </button>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </Disclosure>
  );
};

export default Navigation;
