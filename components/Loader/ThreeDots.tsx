import React from "react";
import { ThreeDots } from "react-loader-spinner";

const ThreeDotsLoader: React.FC<{
  color?: string;
  height?: number;
  width?: string;
}> = ({ color = "#312E81", height = 20, width = 20 }) => {
  return <ThreeDots color={color} height={height} width={width} />;
};

export default ThreeDotsLoader;
