import React from "react";

const ProgressBar: React.FC<{
  width?: number;
  color?: string;
  isDark?: boolean;
}> = ({ width = 100, color = "black", isDark = false }) => {
  return (
    <div className={`w-full flex h-2 sm:rounded`}>
      <div
        className={`bg-${color} dark:dark-grey-1 h-2 sm:rounded mr-1`}
        style={{
          width: `${width}%`,
          backgroundColor: !isDark ? "black" : "#4D4E50",
        }}
      ></div>
      <div
        className={`bg-gray-200 dark:dark-grey-2 h-2 sm:rounded`}
        style={{
          width: `${100 - width}%`,
          backgroundColor: !isDark ? "#e5e7eb" : "#D1D5DB",
        }}
      ></div>
    </div>
  );
};

export default ProgressBar;
