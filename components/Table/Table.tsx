import classNames from "classnames";
import React from "react";
import { useTable } from "react-table";

const Table: React.FC<{
  children?: any;
  columns: any;
  data: any;
  hideHeaderBorder?: boolean;
  onHeaderClick?: Function;
}> = ({
  children,
  columns,
  data,
  hideHeaderBorder = true,
  onHeaderClick = () => {},
}) => {
  const { getTableProps, getTableBodyProps, headerGroups, prepareRow, rows } =
    useTable({
      columns,
      data,
    });

  return (
    <div className="flex flex-col mt-4 mb-4">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
          <div className="overflow-hidden">
            <table className="min-w-full" {...getTableProps()}>
              <thead>
                {headerGroups?.map(
                  (headerGroup: any, headerGroupIndex: number) => (
                    <tr
                      key={`table-tr-${headerGroupIndex}`}
                      {...headerGroup.getHeaderGroupProps()}
                      className={classNames(
                        hideHeaderBorder? "" : "border-gray-50 border-b-2 dark:border-dark-mode"
                      )}
                    >
                      {headerGroup?.headers?.map(
                        (column: any, columnIndex: number) => (
                          <th
                            key={`table-th-${columnIndex}`}
                            scope="col"
                            className="px-2 py-2 text-left text-xs font-bold text-gray-500 dark:text-white uppercase tracking-wider"
                            {...column.getHeaderProps()}
                            onClick={() => onHeaderClick(column)}
                          >
                            {column.render("Header")}
                          </th>
                        )
                      )}
                    </tr>
                  )
                )}
              </thead>
              <tbody {...getTableBodyProps()}>
                {rows?.length > 0 ? (
                  <>
                    {rows?.map((row: any, i: number) => {
                      prepareRow(row);
                      return (
                        <tr
                          key={`table-body-tr-${i}`}
                          {...row.getRowProps()}
                          className={
                            i !== rows.length - 1
                              ? "border-gray-50 border-b-2 dark:border-dark-mode"
                              : ""
                          }
                        >
                          {row?.cells?.map((cell: any, cellIndex: number) => {
                            return (
                              <td
                                key={`table-body-td-${cellIndex}`}
                                {...cell.getCellProps()}
                                className="px-2 py-3 whitespace-nowrap text-sm text-gray-500 dark:text-dark-grey"
                              >
                                {cell.render("Cell")}
                              </td>
                            );
                          })}
                        </tr>
                      );
                    })}
                  </>
                ) : (
                  <tr>
                    <td
                      className="px-6 py-3 whitespace-nowrap text-sm text-gray-500 dark:text-dark-grey text-center"
                      colSpan={columns?.length}
                    >
                      There are no results at this time
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Table;
