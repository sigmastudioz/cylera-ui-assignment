import React, { useState } from "react";
import { ResponsivePie } from "@nivo/pie";

interface ChartProps {
  id: string;
  label: string;
  value: number;
  color: string;
}

const Chart: React.FC<{ data?: any }> = ({ data }) => {
  return (
    <ResponsivePie
      data={data}
      margin={{ top: 60, right: 60, bottom: 60, left: 60 }}
      innerRadius={0.90}
      padAngle={2}
      cornerRadius={15}
      activeOuterRadiusOffset={8}
      colors={{ scheme: "set1" }}
      borderWidth={2}
      borderColor="transparent"
      enableArcLinkLabels={false}
      arcLinkLabelsSkipAngle={10}
      arcLinkLabelsTextColor="#333333"
      arcLinkLabelsThickness={2}
      arcLinkLabelsColor={{ from: "color" }}
      enableArcLabels={false}
      arcLabelsRadiusOffset={0.4}
      arcLabelsSkipAngle={5}
      arcLabelsTextColor={{
        from: "color",
        modifiers: [["darker", 2]],
      }}
    />
  );
};

export default Chart;
