import React from "react";

const HeadLineText: React.FC<{
  title?: any;
  description?: any;
  leftContent?: any;
  rightContent?: any;
}> = ({ title, description, leftContent, rightContent }) => {
  return (
    <div className="flex sm:items-center sm:justify-between">
      {leftContent && <div className="flex space-x-3">{leftContent}</div>}
      <div className="flex-1 min-w-0">
        {title && (
          <h1 className="text-l font-semibold tracking-tight text-gray-900 dark:text-white sm:text-2xl">
            {title}
          </h1>
        )}
        {description && (
          <p className="text-sm text-gray-500 dark:dark-grey">{description}</p>
        )}
      </div>
      {rightContent && <div className="flex space-x-3">{rightContent}</div>}
    </div>
  );
};

export default HeadLineText;
