import React, { useEffect, useState, useRef } from "react";
import type { NextPage, GetServerSideProps } from "next";

import dynamic from "next/dynamic";

import useAsyncEffect from "use-async-effect";

import { ChevronRightIcon, SearchIcon } from "@heroicons/react/solid";
import {
  ArrowSmDownIcon,
  ArrowsExpandIcon,
  ArrowSmUpIcon,
  ArchiveIcon,
  QuestionMarkCircleIcon,
  ScaleIcon,
  UserCircleIcon,
} from "@heroicons/react/outline";

import Card from "@components/Card/Card";
import HeadLineText from "@components/HeadLineText/HeadLineText";
import Table from "@components/Table/Table";
import ProgressBar from "@components/ProgressBar/ProgressBar";
import Navigation from "@components/Navigation/Navigation";

import { Launches, Missions, Payloads } from "@apollo-client/query";
import { ascendingOrder, payloadData, payloadFilter } from "@utils/helpers";
import { capitalize } from "lodash";
import classNames from "classnames";

const Chart: any = dynamic(
  (): Promise<any> => import("@components/Chart/Chart"),
  {
    ssr: false,
  }
);

const ThreeDots: any = dynamic(
  (): Promise<any> => import("@components/Loader/ThreeDots"),
  {
    ssr: false,
  }
);

const launchVariables = {
  sort: "launch_date_utc",
  order: "desc",
  limit: 10,
  offset: 0,
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const payloads = await Payloads();
  const missions = await Missions();
  const launches = await Launches(launchVariables);

  return {
    props: {
      payloads: payloads.data.payloads,
      missions: missions.data.missions,
      launches: launches.data.launches,
    },
  };
};

let searchingIsWorking: NodeJS.Timeout;

const Home: NextPage<{
  payloads: [payloadData];
  missions: any;
  launches: [];
}> = ({ payloads, missions, launches }) => {
  const [showOverview, setShowOverview] = useState(true);
  const [searchByMissionName, setSearchByMissionName] = useState<string | null>(
    ""
  );
  const [allLaunches, setAllLaunches] = useState([]);
  const [launchLoading, setLaunchLoading] = useState(false);
  const [isDark, setIsDark] = useState(false);
  const [overviewStyles, setOverviewStyles] = useState({});
  const overviewContent = useRef<HTMLHeadingElement>(null);
  const payloadCountByCountry = payloadFilter(payloads);
  const [paginationData, setPaginationData] = useState(launchVariables);
  const colors = [
    "#e41a1c",
    "#377eb8",
    "#4daf4a",
    "#984ea3",
    "#ff7f00",
    "#ffff33",
    "#a65628",
    "#f781bf",
    "#999999",
  ];
  const payloadsByCountry = payloadCountByCountry?.payloads.map(
    (payload: any, index: number) => {
      return {
        ...payload,
        color: colors[index],
      };
    }
  );

  const launchData = JSON.parse(
    JSON.stringify({
      ...paginationData,
      find: searchByMissionName
        ? {
            mission_name: searchByMissionName,
          }
        : undefined,
    })
  );

  useEffect(() => {
    setOverviewStyles({
      height: `${overviewContent.current?.clientHeight}px`,
      overflow: "none",
      transition: "500ms ease-in-out",
    });

    const theme = window.localStorage.getItem("theme");

    setIsDark(theme === "dark");
  }, []);

  const getLaunchData = async (currentLaunchData: any) => {
    setLaunchLoading(true);
    const updateLaunches = await Launches(currentLaunchData);
    setAllLaunches(updateLaunches.data.launches);
    setLaunchLoading(false);
    setPaginationData(currentLaunchData);
  };

  useAsyncEffect(async () => {
    setLaunchLoading(true);
    if (searchByMissionName === "") {
      setAllLaunches(launches);
      setLaunchLoading(false);

      return;
    }

    getLaunchData({
      ...launchData,
      offset: 0,
    });
  }, [searchByMissionName]);

  const searchTerm = (value: string) => {
    clearTimeout(searchingIsWorking);
    searchingIsWorking = setTimeout(function () {
      setSearchByMissionName(value?.length ? value : null);
    }, 350);
  };

  let payloadCount = 0;
  let payloadTotal = 0;

  const topMissions = missions
    .map((mission: any) => {
      const payload_mass_kg = mission.payloads
        ?.filter((payload: any) => payload?.payload_mass_kg)
        ?.map((payload: any) => payload?.payload_mass_kg)
        ?.reduce((a: number, b: number) => a + b);

      payloadCount++;
      payloadTotal += payload_mass_kg;
      return {
        name: mission.name,
        payload_mass_kg,
      };
    })
    ?.sort((a: any, b: any) => ascendingOrder(a, b, "payload_mass_kg"))
    ?.splice(0, 5);

  const biggestPayload = topMissions[0] || 0;
  const payloadAvg = payloadTotal / payloadCount;

  const formatLaunchHeader = (
    text: string,
    className?: string,
    sortBy?: string
  ) => {
    const sortValue = sortBy || text?.toLowerCase()?.replace(/\s/g, "_");
    const arrowClass = "text-black dark:text-white h-4 w-4 ml-2";
    return (
      <div
        className={`${className} flex capitalize pb-4 -mb-2`}
        onClick={() => {
          getLaunchData({
            ...launchData,
            sort: sortValue,
            order: paginationData.order === "asc" ? "desc" : "asc",
            offset: 0,
          });
        }}
      >
        {text}
        {paginationData.sort === sortValue && (
          <span>
            {paginationData.order === "asc" ? (
              <ArrowSmUpIcon className={arrowClass} />
            ) : paginationData.order === "desc" ? (
              <ArrowSmDownIcon className={arrowClass} />
            ) : null}
          </span>
        )}
      </div>
    );
  };

  const missionsSummary = [
    {
      icon: (
        <ArchiveIcon className="text-green-600 h-6 w-6 mr-2 -mt-4"></ArchiveIcon>
      ),
      title: `${payloadCountByCountry.total}`,
      description: "Total Payloads",
    },
    {
      icon: (
        <ScaleIcon className="text-indigo-500 h-6 w-6 mr-2 -mt-4"></ScaleIcon>
      ),
      title: `${payloadAvg.toFixed(2)} Kg`,
      description: "Avg. Payload Mass",
    },
    {
      icon: (
        <UserCircleIcon className="text-yellow-500 h-6 w-6 mr-2 -mt-4"></UserCircleIcon>
      ),
      title: `${payloadCountByCountry.customers.length}`,
      description: "Total Payload Customers",
    },
  ];

  const payloadChartData = payloadsByCountry?.map((payload: any) => {
    return {
      id: payload?.nationality?.toLowerCase(),
      label: capitalize(payload.nationality),
      value: payload.count,
      color: payload.color,
    };
  });

  return (
    <div className={isDark ? "dark" : ""}>
      <div
        className={"bg-gray-50 dark:bg-dark-mode min-h-screen relative pt-8"}
      >
        <Navigation
          isDark={isDark}
          toggleDark={(mode: boolean) => {
            window.localStorage.setItem("theme", mode ? "dark" : "light");
            setIsDark(mode);
          }}
        />
        <div className="max-w-7xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:px-8">
          <div className="w-full" style={overviewStyles}>
            <div className="w-full" ref={overviewContent}>
              <div className="mt-2 grid grid-cols-1 gap-5 sm:grid-cols-2 lg:grid-cols-3">
                {missionsSummary.map((mission, index) => {
                  return (
                    <div
                      key={`${mission.title}-${index}`}
                      className="bg-gray-200 dark:bg-dark-coal px-6 py-4 drop-shadow-md sm:rounded-lg sm:overflow-hidden"
                    >
                      <HeadLineText
                        title={
                          <span className="sm:text-xl">{mission.title}</span>
                        }
                        description={mission.description}
                        leftContent={mission.icon}
                        rightContent={
                          <ChevronRightIcon className="text-black dark:text-white h-6 w-6"></ChevronRightIcon>
                        }
                      />
                    </div>
                  );
                })}
              </div>

              <div className="mt-6 grid grid-cols-1 gap-y-6 sm:grid-cols-1 md:grid-cols-2 md:grid-rows-1 sm:gap-x-2 lg:gap-5">
                <div className="group aspect-w-2 aspect-h-1 rounded-lg overflow-hidden sm:aspect-h-1 sm:aspect-w-1 sm:row-span-2">
                  <Card
                    header={
                      <HeadLineText
                        title={
                          <div className="flex">
                            <div className="sm:text-xl">
                              Payload Count By Nationality
                            </div>
                            <div className="flex flex-col justify-center pl-2">
                              <QuestionMarkCircleIcon className="text-black dark:text-white h-4 w-4"></QuestionMarkCircleIcon>
                            </div>
                          </div>
                        }
                      />
                    }
                  >
                    <div className={"flex"}>
                      <div className="w-2/4">
                        <Chart data={payloadChartData} />
                      </div>
                      <div className="w-2/4">
                        <div className={"w-full pr-6"}>
                          <Table
                            columns={[
                              {
                                Header: "Nationality",
                                accessor: "nationality",
                                Cell: (props: { value: string; row: any }) => {
                                  return (
                                    <div className="flex items-center space-x-3 lg:pl-2">
                                      <div
                                        className={
                                          "flex-shrink-0 w-2.5 h-2.5 rounded-full"
                                        }
                                        aria-hidden="true"
                                        style={{
                                          backgroundColor:
                                            props.row.original.color,
                                        }}
                                      />
                                      <span>{props.value}</span>
                                    </div>
                                  );
                                },
                              },
                              {
                                Header: "Payload Count",
                                accessor: "count",
                              },
                            ]}
                            data={payloadsByCountry}
                          ></Table>
                        </div>
                      </div>
                    </div>
                  </Card>
                </div>

                <div className="group aspect-w-2 aspect-h-1 rounded-lg overflow-hidden sm:relative sm:aspect-none">
                  <Card
                    header={
                      <HeadLineText
                        title={
                          <div className="flex">
                            <div className="sm:text-xl">Top 5 Missions</div>
                            <div className="flex flex-col justify-center pl-2">
                              <QuestionMarkCircleIcon className="text-black dark:text-white h-4 w-4"></QuestionMarkCircleIcon>
                            </div>
                          </div>
                        }
                      />
                    }
                  >
                    <div className={"flex"}>
                      <div className={"w-full px-6"}>
                        <Table
                          columns={[
                            {
                              Header: "Mission",
                              accessor: "name",
                              Cell(props: { value: string }) {
                                return (
                                  <div
                                    className={
                                      "text-black dark:text-dark-grey font-semibold"
                                    }
                                  >
                                    {props.value}
                                  </div>
                                );
                              },
                            },
                            {
                              Header: "Payload Mass",
                              accessor: "payload_mass_kg",
                              Cell(props: { value: number }) {
                                return (
                                  <div
                                    className={"flex"}
                                    style={{ minWidth: "160px" }}
                                  >
                                    <div className="w-1/3">
                                      {props.value} kg
                                    </div>
                                    <div className="w-2/3 flex flex-col justify-center">
                                      <ProgressBar
                                        isDark={isDark}
                                        width={
                                          (props.value /
                                            biggestPayload.payload_mass_kg) *
                                          100
                                        }
                                      />
                                    </div>
                                  </div>
                                );
                              },
                            },
                          ]}
                          data={topMissions}
                        ></Table>
                      </div>
                    </div>
                  </Card>
                </div>
              </div>
            </div>
          </div>

          <div className="mt-6">
            <Card
              header={
                <HeadLineText
                  title={<div className="sm:text-xl"> SpaceX Launch Data</div>}
                  rightContent={
                    <ArrowsExpandIcon
                      className="text-blue-400 dark:text-white h-6 w-6"
                      onClick={() => {
                        const updateShowOverview = !showOverview;
                        setShowOverview(updateShowOverview);
                        setOverviewStyles({
                          ...overviewStyles,
                          height: updateShowOverview
                            ? `${overviewContent.current?.clientHeight}px`
                            : 0,
                          overflow: updateShowOverview ? "none" : "hidden",
                        });
                      }}
                    ></ArrowsExpandIcon>
                  }
                />
              }
            >
              <div className="px-4 py-6 sm:px-6">
                <label htmlFor="search" className="sr-only">
                  Search By Mission Name
                </label>
                <div className="mt-1 relative rounded-md bg-gray-50 dark:bg-dark-coal">
                  <div
                    className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none"
                    aria-hidden="true"
                  >
                    <SearchIcon
                      className="mr-3 h-4 w-4 text-gray-400"
                      aria-hidden="true"
                    />
                  </div>
                  <input
                    type="text"
                    name="search"
                    id="search"
                    className="focus:ring-gray-500 focus:border-gray-500 block w-full pl-9 py-2 sm:text-sm border-gray-300 dark:text-white rounded-md bg-transparent"
                    placeholder="Search By Mission Name"
                    onChange={(e) => searchTerm(e?.target?.value)}
                  />

                  {launchLoading && (
                    <div
                      className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none"
                      aria-hidden="true"
                    >
                      <ThreeDots color="#3b82f6" height={20} width={20} />
                    </div>
                  )}
                </div>
              </div>
              <div className="pb-8">
                <Table
                  columns={[
                    {
                      Header: () => formatLaunchHeader("Mission Name", "pl-4"),
                      accessor: "mission_name",
                      Cell(props: { value: string }) {
                        return <div className="pl-4">{props.value}</div>;
                      },
                    },
                    {
                      Header: () => formatLaunchHeader("Date", "", "launch_date_utc"),
                      accessor: "launch_date_utc",
                    },
                    {
                      Header: () => formatLaunchHeader("Outcome", "", "launch_success"),
                      accessor: "launch_success",
                      Cell(props: { value: string }) {
                        return (
                          <div
                            className={`${
                              props.value ? "text-green-500" : "text-red-500"
                            } font-semibold`}
                          >
                            {props.value ? "Success" : "Failure"}
                          </div>
                        );
                      },
                    },
                    {
                      Header: () => formatLaunchHeader("Rocket", "", "rocket_name"),
                      accessor: "rocket.rocket_name",
                    },
                    {
                      Header: () => formatLaunchHeader("Payload Mass", "", "kg"),
                      accessor: "rocket.rocket.mass.kg",
                      Cell(props: { value: string }) {
                        return <div>{props.value} kg</div>;
                      },
                    },
                    {
                      Header: () => formatLaunchHeader("Site", "", "site_id"),
                      accessor: "launch_site.site_id",
                      Cell(props: { value: string }) {
                        return (
                          <div>
                            {props.value?.replace(/_/g, " ")?.toUpperCase()}
                          </div>
                        );
                      },
                    },
                    {
                      Header: () => formatLaunchHeader("Mission ID", "pr-4"),
                      accessor: "mission_id",
                      Cell(props: { value: string }) {
                        return <div className="pr-4">{props.value}</div>;
                      },
                    },
                  ]}
                  data={allLaunches}
                  hideHeaderBorder={false}
                ></Table>
                <div
                  className="px-4 py-3 flex items-center justify-between border-t border-gray-200 dark:border-dark-mode sm:px-6"
                  aria-label="Pagination"
                >
                  <div className="flex-1 flex justify-between sm:justify-end">
                    <button
                      className={classNames(
                        0 === paginationData.offset ? "opacity-50" : "",
                        "relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
                      )}
                      onClick={() => {
                        getLaunchData({
                          ...launchData,
                          offset:
                            paginationData.offset - paginationData.limit - 1,
                        });
                      }}
                      disabled={0 === paginationData.offset}
                    >
                      Previous
                    </button>
                    <button
                      className={classNames(
                        allLaunches.length < paginationData.limit
                          ? "opacity-50"
                          : "",
                        "ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
                      )}
                      disabled={allLaunches.length < paginationData.limit}
                      onClick={() => {
                        getLaunchData({
                          ...launchData,
                          offset:
                            paginationData.limit + paginationData.offset + 1,
                        });
                      }}
                    >
                      Next
                    </button>
                  </div>
                </div>
              </div>
            </Card>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
